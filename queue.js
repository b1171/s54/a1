let collection = [];

// Write the queue functions below.
function enqueue(item) {
	collection[collection.length] = item;
	return collection;
}

function dequeue() {
	let dequeuedArray = [];
	for(let i=1; i<collection.length; i++){
		dequeuedArray[i-1] = collection[i];
	}
	collection = dequeuedArray;
	return collection;
}

function print() {
	return collection;
}

function front() {
	return collection[0];
}

function size() {
	return collection.length;
}

function isEmpty() {
	return collection.length == 0 ? true : false;
}

module.exports = {
	enqueue,
	dequeue,
	print,
	front,
	size,
	isEmpty
};